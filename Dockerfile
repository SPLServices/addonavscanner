FROM splservices/baseimage

RUN apt-get update -y && \
  apt-get install clamav curl nodejs npm -y &&\
  apt clean; rm -rf /var/lib/apt/lists/*

RUN freshclam

RUN npm install -g tap-xunit
RUN npm install -g bats

COPY addonscanner /usr/local/bin/addonscanner
COPY entrypoint.sh /usr/local/bin/entrypoint.sh

RUN chmod +x /usr/local/bin/addonscanner
RUN chmod +x /usr/local/bin/entrypoint.sh

VOLUME ["/usr/src"]
VOLUME ["/usr/results"]

WORKDIR /usr/src
#ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
